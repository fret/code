"""A basic GUI application using tkinter."""
import tkinter
import tkinter.messagebox as messagebox


class GUIFrame(tkinter.Frame):
    """This is the GUI"""

    def __init__(self):
        """Initialize yourself"""

        # Initialise the base class
        # super().__init__(self, master=tkinter.Tk())
        super().__init__(tkinter.Tk())

        # "Set the Window Title
        self.master.title("Type Some Text")

        # Create empty variables for the widgets
        self.lbl = self.entry = self.button = self.quit = None

        # Display the main window with a little bit of padding
        self.grid(padx=10, pady=10)
        self.create_widgets()

    def create_widgets(self):
        """Create all the widgets that we need"""

        # Create the Text
        self.lbl = tkinter.Label(self, text="Enter Text:")
        self.lbl.grid(row=0, column=0)

        # Create the Entry, set it to be a bit wider
        self.entry = tkinter.Entry(self)
        self.entry.grid(row=0, column=1, columnspan=3)

        # Create the Button, set the text and the command that will be called
        # when the button is clicked
        self.button = tkinter.Button(self, text="Display!",
                                     command=self.display)
        self.button.grid(row=0, column=4)

        self.quit = tkinter.Button(self, text="Quit",
                                   command=self.master.destroy)
        self.quit.grid(row=1, column=4)
        self.master.focus()

    def display(self):
        """Called by button, displays text from the entry widget."""
        messagebox.showinfo("Text", "You typed: %s" % self.entry.get())


if __name__ == "__main__":
    GUIFrame().mainloop()
