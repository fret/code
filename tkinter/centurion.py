"""
Centurion: An app for functional alcoholics.

"""

import tkinter


class Globals(object):
    """A container for game constants."""

    window_title_template = "Centurion: %s"

    # When set this determines the next window to open.
    next_window = None


def cli_main():
    """Parse the command line and start the game."""


class Centurion(tkinter.Tk):
    """The Main Centurion App for functional alcoholics."""

    def __init__(self):
        """Prep the app and the first game window."""
        super().__init__()

        Globals.next_window = Disclaimer
        self.load_window(Disclaimer)

    def load_window():
        """Load and run the specified window class."""
        window = window_class(self.root)

        window.mainloop()

class Disclaimer(tkinter.Frame):
    """The Starting Disclaimer window for the Centurion Drinking game."""

    def __init__(self, master=None):
        """Load the window."""
        super().__init__(master)

        # "Set the Window Title
        self.master.title("Centurion: Disclaimer")

        # Create empty variables for the widgets
        self.lbl = self.entry = self.accept = self.quit = None

        # Display the main window with a little bit of padding
        self.grid(padx=10, pady=10)
        self.create_widgets()

    def create_widgets(self):
        """Create all the widgets that we need"""

        # Create the Text
        self.lbl = tkinter.Label(
            self, text="Disclaimer text goes here.")
        self.lbl.grid(row=0, column=0)

        # Create the Entry, set it to be a bit wider
        self.entry = tkinter.Entry(self)
        self.entry.grid(row=1, column=1, columnspan=3)

        # Create the Button, set the text and the command that will be called
        # when the button is clicked
        self.button = tkinter.Button(self, text="Display!",
                                     command=self.display)
        self.button.grid(row=2, column=4)

        self.quit = tkinter.Button(self, text="Quit",
                                   command=self.master.destroy)
        self.quit.grid(row=4, column=4)
        self.master.focus()


if __name__ == "__main__":
    cli_main()

# -----

DESIGN = """
Centurion Design Notes
======================

Start Window
------------

Print a basic disclaimer to the user and get them to confirm they understand
the risks of drinking games. Provide an Accept and a Quit button. Accept

"""
