#!/bin/bash

# Run speedtest-cli and get just the download and upload speed outputs

date +%Y%m%d%H%M
speedtest-cli | grep -e Download -e Upload
echo
