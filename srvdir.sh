#!/bin/bash
# Copyright 2018 Curtis Sand
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PORT="8080";
DIR="$(pwd)";

SCRIPT=$0

usage () {
    echo "$SCRIPT [-h|--help] [DIR]";
    echo -e "\n  Serve a directory of files in an nginx docker image"
    echo "      and open Google Chrome if it can be found."
    echo "      If docker requires a password you may need to"
    echo "      refresh the Chrome browser window that is opened."
    echo -e "\n  Options:"
    echo "    -h|--help Print this help message."
    echo "    DIR       (optional) serve DIR instead of CWD."
    local rc=0
    [ -n "$1" ] && rc=$1
    exit $rc
}

for arg in $@; do
    if [[ "${arg:0:2}" == "-h" ]] || [[ "${arg:0:3}" == "--h" ]]; then
        usage;
    fi
done

# Use dir from command line if available otherwise use default
if [[ $# -ge 1 ]]; then
    DIR="$1"
fi

# Hacky tests for MacOS vs Linux to open Google Chrome
which google-chrome &> /dev/null
USE_PATH=$?
test -e /Applications/Safari.app
USE_MAC_OPEN=$?
if [[ $USE_PATH -eq 0 ]]; then  # use chrome on path
    echo "running chrome from path...";
    google-chrome --app=http://localhost:$PORT $(sleep 1) &
elif [[ $USE_MAC_OPEN -eq 0 ]]; then  # on MacOS, use Open
    echo "running chrome with 'open'...";
    open -a "Google Chrome" --new "http://localhost:$PORT" $(sleep 1) &
else
    echo "chrome not found, skipping..."
fi

sudo docker run --rm -p ${PORT}:80 -v ${DIR}:/usr/share/nginx/html/:ro nginx
